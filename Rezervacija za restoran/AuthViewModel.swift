//
//  AuthViewModel.swift
//  Rezervacija za restoran
//
//  Created by student on 26.01.2024..
//

import Foundation
import Firebase
import FirebaseFirestoreSwift

protocol AuthenticationFormProtocol{
    var formIsValid: Bool { get }
}


@MainActor
class AuthViewModel : ObservableObject{
    
    let date_url = URL(string: "https://baza-za-korisnike-default-rtdb.europe-west1.firebasedatabase.app/dates.json")!

    
    
    @Published var userSession: FirebaseAuth.User?
    @Published var currentUser: User?
   
    @Published var dates: [Dates] = []
    
    func getDates(inds: [String]) -> [Dates]{
           return dates.filter { date in
               return inds.contains(date.id)
           }
       }

    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        return formatter
    }()
    
    init(){
       self.userSession = Auth.auth().currentUser
       
        Task{
            await fetchUser()
        }
    }
    func sendDate(date: Dates) async
        {
            do {
                let encoder = JSONEncoder()
                encoder.dateEncodingStrategy = .iso8601
                let json = try encoder.encode(date)
                
                
                var request = URLRequest(url: date_url)
                request.httpMethod = "POST"
                request.httpBody = json
                
                let (_, response) = try await URLSession.shared.data(for: request)
                print(response)
            }catch let error {
                print(error)
            }
        }

    func fetchDates() async
        {
            do {
                let (data, _) = try await URLSession.shared.data(from: date_url)
                
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                
                let decoded_dates = try decoder.decode([String: Dates].self, from: data)
                dates = [Dates](decoded_dates.values)
            } catch let error {
                print(error)
            }
        }

    
    func signIn(withEmail email: String, password: String) async throws{
        do{
            let result = try await Auth.auth().signIn(withEmail: email, password: password)
            self.userSession = result.user
            await fetchUser()
        }catch{
            print("DEBUG: Failed to log in with error \(error.localizedDescription)")
        }
    }
    
    func signOut(){
        do{
            try Auth.auth().signOut()
            self.userSession=nil
            self.currentUser=nil
        } catch{
            print("DEBUG: Failed to sign out with error \(error.localizedDescription)")
        }
    }
    
    
    func createUser(withEmail email: String, password: String, fullname: String) async throws{
        do{
            let result = try await Auth.auth().createUser(withEmail: email, password: password)
            self.userSession = result.user
            let user = User(id: result.user.uid, fullname: fullname, email:email)
            let encodedUser = try Firestore.Encoder().encode(user)
            try await Firestore.firestore().collection("users").document(user.id).setData(encodedUser)
            await fetchUser()
        } catch{
            print("DEBUG: Failed to create user with error\(error.localizedDescription)")
        }
    }
    
    // func saveDate(date: Date) async throws{
     //   do{
     //       let userID : String = (Auth.auth().currentUser?.uid)!
     //       let date = Dates(id: userID, date: Date())
     //       let encodedDate = try Firestore.Encoder().encode(date)
     //      try await Firestore.firestore().collection("dates").document(date.id).setData(encodedDate)
            //await fetchDate()
    //    } catch{
    //        print("DEBUG: Failed to save date with error\(error.localizedDescription)")
    //    }
  //   }
    
    
     // func fetchDate() async{
      //    guard let uid=Auth.auth().currentUser?.uid else { return }
    
     //   guard let snapshot = try? await Firestore.firestore().collection("dates").document(uid).getDocument() else {return}
     //   self.currentUser = try? snapshot.data(as: User.self)
    
    //   print("DEBUG: Current date is \(self.currentUser)")
    // }
    
    func fetchUser() async{
        guard let uid=Auth.auth().currentUser?.uid else { return }
        
        guard let snapshot = try? await Firestore.firestore().collection("users").document(uid).getDocument() else {return}
        self.currentUser = try? snapshot.data(as: User.self)
        
        print("DEBUG: Current user is \(self.currentUser)")
    }
    
}
