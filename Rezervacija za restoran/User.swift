//
//  User.swift
//  Rezervacija za restoran
//
//  Created by student on 26.01.2024..
//

import Foundation

struct User: Identifiable, Codable{
    let id: String
    let fullname: String
    let email: String
    var date: Date = Date()
    
    var initals: String{
        let formatter = PersonNameComponentsFormatter()
        if let components = formatter.personNameComponents(from: fullname){
            formatter.style = .abbreviated
            return formatter.string(from:components)
        }
        
        return ""
    }
}
