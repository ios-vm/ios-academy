//
//  DateRow.swift
//  Rezervacija za restoran
//
//  Created by student on 10.02.2024..
//

import SwiftUI

struct DateRow: View {
    
    @Binding var date: Dates
    
    func getCurrentDateTime(date: Date)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        return dateFormatter.string(from: date)
    }
    
    var body: some View {
        HStack{
            VStack(alignment: .leading){
                Text(getCurrentDateTime(date: date.date))
                    .foregroundColor(.gray)
                    .font(.subheadline)
                    .padding(.bottom, 1)
            }
            .padding(.leading)
            Spacer()
            
            }
        }
    }

