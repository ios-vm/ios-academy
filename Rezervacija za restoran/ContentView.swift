//
//  ContentView.swift
//  Rezervacija za restoran
//
//  Created by student on 23.01.2024..
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var viewModel: AuthViewModel
    
    var body: some View {
        Group{
            if viewModel.userSession != nil {
                ProfileView()
            }else{
                LoginView()
            }
        }
        
    }
}


#Preview {
    ContentView()
}
