//
//  ProfileView.swift
//  Rezervacija za restoran
//
//  Created by student on 26.01.2024..
//

import SwiftUI


struct ProfileView: View {
    @State private var selectedDate: Date = Date()
    @State private var isPresented = false
    @State private var isPresented2 = false
    
    
    
    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        return formatter
    }()
    
    func getCurrentDateTime(date: Date)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        return dateFormatter.string(from: date)
    }
    
    @EnvironmentObject var viewModel: AuthViewModel
    @EnvironmentObject var userData: UserData
    
    
    
    var body: some View {
        
        //   ProfileView().environmentObject(rezervationData)
        
        if let user = viewModel.currentUser{
            
            
            List {
                Section{
                    HStack {
                        Text(user.initals)
                            .font(.title)
                            .fontWeight(.semibold)
                            .foregroundColor(.white)
                            .frame(width:72, height: 72)
                            .background(Color(.systemGray3))
                            .clipShape(Circle())
                        
                        VStack(alignment: .leading, spacing: 4){
                            Text(user.fullname)
                                .fontWeight(.semibold)
                                .padding(.top,4)
                            
                            Text(user.email)
                                .font(.footnote)
                                .accentColor(.gray)
                        }
                    }
                }
                
                Section("Make your reservation"){
                    DatePicker(
                        "Pick date:",
                        selection: $selectedDate,
                        displayedComponents: [.date, .hourAndMinute]
                    )
                    
                    .datePickerStyle(.graphical)
                }
                Text(getCurrentDateTime(date: selectedDate))
                
                
                Section(){
                    Button(action : {isPresented = true}){
                        HStack{
                            Image(systemName:"calendar")
                            Text("CONFIRM RESERVATION")
                        }
                    }
                    
                }.sheet(isPresented: $isPresented){
                    VStack{
                        HStack{
                            Text("Reserved Date: \(selectedDate, formatter: dateFormatter)")
                                .padding()
                        }
                        
                        Button(action: {
                            Task{
                                
                                let dateToSend = Dates(date: selectedDate, email: user.email, username: user.fullname)
                                await viewModel.sendDate(date: dateToSend)
                                // userData.myDatesIds.append(dateToSend)
                                await viewModel.fetchDates()
                            }
                        }, label: {
                            SettingsRowView(imageName:"checkmark", title:"Confirm date", tintColor:.green)
                        })
                        
                    }
                }
                Section(){
                    Button(action : {isPresented2 = true}){
                        HStack{
                            Image(systemName:"calendar")
                            Text("My reservations")
                        }
                    }
                    
                }.sheet(isPresented: $isPresented2){
                    
                    
                }
                    Section("Account"){
                        Button{
                            viewModel.signOut()
                        } label:{
                            SettingsRowView(imageName: "arrow.left.circle.fill",title:"Sign out", tintColor:.red)
                        }
                        
                        
                        
                    }
                    
                }.listSectionSpacing(.compact)
            }
            
        }
        
    }

    
    #Preview {
        ProfileView()
    }
    

