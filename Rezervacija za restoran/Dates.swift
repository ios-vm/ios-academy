//
//  Dates.swift
//  Rezervacija za restoran
//
//  Created by student on 05.02.2024..
//

import Foundation

struct Dates: Identifiable, Codable {
    var id = UUID().uuidString
    var date: Date = Date()
    var email: String
    var username: String
}

