//
//  Rezervacija_za_restoranApp.swift
//  Rezervacija za restoran
//
//  Created by student on 23.01.2024..
//

import SwiftUI
import Firebase
@main
struct Rezervacija_za_restoranApp: App {
    @StateObject var viewModel = AuthViewModel()
    
    init(){
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(viewModel)
        }
    }
}
